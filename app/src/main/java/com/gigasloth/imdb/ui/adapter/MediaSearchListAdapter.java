package com.gigasloth.imdb.ui.adapter;

import android.graphics.Bitmap;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.gigasloth.imdb.Imdb;
import com.gigasloth.imdb.R;
import com.gigasloth.imdb.obj.Media;
import com.gigasloth.imdb.ui.event.MediaClickEvent;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MediaSearchListAdapter extends RecyclerView.Adapter<MediaSearchListAdapter.MediaViewHolder> {
    private ArrayList<Media> mediaList = new ArrayList<>();
    private Media media;

    public MediaSearchListAdapter(ArrayList<Media> mediaList) {
        this.mediaList = mediaList;
        Imdb.getBus().register(this);
    }

    @Override
    public int getItemCount() {
        return mediaList.size();
    }

    /**
     * This is where we bind the data to the views. This is called when the SO binds the view with
     * the data (when it's shown in the UI)
     *
     * @param mediaViewHolder
     *      ViewHolder to be bound
     * @param pos
     *      Position in the data list
     */
    @Override
    public void onBindViewHolder(MediaViewHolder mediaViewHolder, int pos) {
        media = mediaList.get(pos);
        mediaViewHolder.holderObj = media;
        mediaViewHolder.tvTitle.setText(media.getTitle());
        mediaViewHolder.tvReleaseDate.setText(media.getReleaseDate());
        mediaViewHolder.imgPosterImage.setImageBitmap(null);
        mediaViewHolder.holderObj.setWatched(Imdb.getDbHelper().isMediaWatched(media));

        if (media.getPosterPath() != null) {
            getPosterImage(media, mediaViewHolder, pos);
        } else {
            mediaViewHolder.imgPosterImage.setAlpha(.54f);
            mediaViewHolder.imgPosterImage.setImageBitmap(null);
        }

        if (mediaViewHolder.holderObj.isWatched()) {
            mediaViewHolder.imgWatched.setVisibility(View.VISIBLE);
        } else {
            mediaViewHolder.imgWatched.setVisibility(View.GONE);
        }
    }

    /**
     * <code>ImageLoader</code> request for poster images. Loads the image from url or from cache if available.
     *
     * @param media
     *          The media object
     *
     * @param mediaViewHolder
     *          The image view to set the poster image
     */
    private void getPosterImage(Media media, final MediaViewHolder mediaViewHolder, final int adapterPos) {
        Imdb app = Imdb.getInstance();
        String requestUrl = Imdb.config.getSecureBaseUrl() + "/original/" + media.getPosterPath();

        app.getImageLoader().get(requestUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer imageContainer, boolean isImmediate) {
                Bitmap bitmap;

                // ImageLoader's first pass checks the cache which will return null first time around
                if ((bitmap = imageContainer.getBitmap()) == null) {
                    return;
                }

                if (mediaViewHolder.getAdapterPosition() != adapterPos) {
                    return;
                }

                mediaViewHolder.imgPosterImage.setAlpha(1f);
                mediaViewHolder.imgPosterImage.setImageBitmap(bitmap);
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VOLLEY", volleyError.toString());
            }
        });
    }

    /**
     * This method inflates a new card layout and builds a new ViewHolder object. This is called
     * whenever a new instance of our ViewHolder class is created.
     *
     * @param viewGroup
     *      ViewGroup to add the new view to
     * @param pos
     *      Position in the data list
     * @return
     *      A new ViewHolder
     */
    @Override
    public MediaViewHolder onCreateViewHolder(final ViewGroup viewGroup, final int pos) {
        View itemView = LayoutInflater
                .from(viewGroup.getContext())
                .inflate(R.layout.view_list_item_search, viewGroup, false);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                final MediaViewHolder mediaViewHolder = ((MediaViewHolder) v.getTag());

                mediaViewHolder.tvTitle.setText(mediaViewHolder.holderObj.getTitle());
                mediaViewHolder.tvReleaseDate.setText(mediaViewHolder.holderObj.getReleaseDate());
                getPosterImage(mediaViewHolder.holderObj, mediaViewHolder, pos);
            }
        });

        return new MediaViewHolder(itemView);
    }

    public static class MediaViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        @Bind(R.id.tv_title_search) TextView tvTitle;
        @Bind(R.id.tv_release_date_search) TextView tvReleaseDate;
        @Bind(R.id.img_poster_thumbnail_search) ImageView imgPosterImage;
        @Bind(R.id.img_watched) ImageView imgWatched;
        protected Media holderObj;

        public MediaViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
            view.setTag(this);
            tvTitle = (TextView) view.findViewById(R.id.tv_title_search);
            tvReleaseDate = (TextView) view.findViewById(R.id.tv_release_date_search);
            imgPosterImage = (ImageView) view.findViewById(R.id.img_poster_thumbnail_search);

            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            Media media = ((MediaViewHolder) view.getTag()).holderObj;
            media.setWatched(true);
            Imdb.getDbHelper().saveMedia(media);
            Imdb.getBus().post(new MediaClickEvent(getAdapterPosition()));
        }
    }

}
