package com.gigasloth.imdb.ui.activity;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.gigasloth.imdb.Imdb;
import com.gigasloth.imdb.R;
import com.gigasloth.imdb.ui.adapter.ImdbPagerAdapter;
import com.gigasloth.imdb.ui.event.SearchClickedEvent;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ContainerActivity extends AppCompatActivity {

    private FragmentManager fragmentManager;
    private ActionBar actionBar;

    @Bind(R.id.pager) ViewPager pager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_container);
        ButterKnife.bind(this);

        actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setElevation(0);
        }
        actionBar.setTitle(getString(R.string.app_name));

        fragmentManager = getSupportFragmentManager();
        configureViewPager();
    }

    @Override
    public void onResume() {
        super.onResume();
        Imdb.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Imdb.getBus().unregister(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_toolbar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Imdb.getBus().post(new SearchClickedEvent());
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void configureViewPager() {
        pager.setAdapter(new ImdbPagerAdapter(fragmentManager));
        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Imdb.setActiveFragment(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }
}
