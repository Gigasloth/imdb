package com.gigasloth.imdb.ui.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.gigasloth.imdb.Imdb;
import com.gigasloth.imdb.R;
import com.gigasloth.imdb.ui.fragment.MovieSearchFragment;
import com.gigasloth.imdb.ui.fragment.TvSearchFragment;

public class ImdbPagerAdapter extends FragmentPagerAdapter {
    private String tabtitles[] = new String[] {
            Imdb.getInstance().getString(R.string.tab_movies),
            Imdb.getInstance().getString(R.string.tab_tv)
    };
    private final int PAGE_COUNT = 2;

    public ImdbPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    /**
     * This method will be invoked when a page is requested to create
     */
    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                Fragment movieFragment = new MovieSearchFragment();

                return movieFragment;
            case 1:
                Fragment tvFragment = new TvSearchFragment();

                return tvFragment;
        }

        return null;
    }

    /**
     * Returns the number of pages
     */
    @Override
    public int getCount() {
        return PAGE_COUNT;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles[position];
    }
}