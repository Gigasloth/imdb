package com.gigasloth.imdb.ui.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.gigasloth.imdb.Imdb;
import com.gigasloth.imdb.R;
import com.gigasloth.imdb.obj.Media;
import com.gigasloth.imdb.ui.activity.MovieDetailActivity;
import com.gigasloth.imdb.ui.adapter.MediaSearchListAdapter;
import com.gigasloth.imdb.ui.event.MediaClickEvent;
import com.gigasloth.imdb.ui.event.SearchClickedEvent;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import info.movito.themoviedbapi.TmdbSearch;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;

public class MovieSearchFragment extends Fragment implements TextView.OnEditorActionListener {
    private static final String STATE_MOVIE_LIST = "movie_list";

    @Bind(R.id.rv_results) RecyclerView rvResults;
    @Bind(R.id.tv_lv_empty) TextView tvEmptyState;
    @Bind(R.id.pb_loading) ProgressBar pbLoading;
    private EditText etSearch;

    private MediaSearchListAdapter movieAdapter;
    private ArrayList<Media> mediaList;
    private AlertDialog searchDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup root, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_search, root, false);
        ButterKnife.bind(this, view);

        mediaList = new ArrayList<>();
        movieAdapter = new MediaSearchListAdapter(mediaList);

        rvResults.setLayoutManager(new LinearLayoutManager(Imdb.getInstance(),
                LinearLayoutManager.VERTICAL,
                false));
        rvResults.setAdapter(movieAdapter);

        if (savedInstanceState != null) {
            ArrayList<Media> savedList = (ArrayList <Media>) savedInstanceState.getSerializable(STATE_MOVIE_LIST);

            if (savedList != null) {
                mediaList.addAll(savedList);
            }
            checkEmptyState();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Imdb.getBus().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        Imdb.getBus().unregister(this);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putSerializable(STATE_MOVIE_LIST, mediaList);

        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public boolean onEditorAction(TextView textView, int actionId, KeyEvent event) {
        if (actionId == EditorInfo.IME_NULL && event.getAction() == KeyEvent.ACTION_DOWN) {
            if (searchDialog != null) {
                if (!validateSearch()) {
                    searchDialog.dismiss();
                    return false;
                }

                search(etSearch.getText().toString());
                searchDialog.dismiss();
            }

            return true;
        }

        return false;
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onSearchClicked(SearchClickedEvent event) {
        if(Imdb.getActiveFragment() != 0) {
            return;
        }

        showSearchDialog();
    }

    @Subscribe
    @SuppressWarnings("unused")
    public void onMovieSelected(MediaClickEvent event) {
        if(Imdb.getActiveFragment() != 0) {
            return;
        }

        movieAdapter.notifyDataSetChanged();

        Intent detailActivityIntent = new Intent(getActivity(), MovieDetailActivity.class);
        detailActivityIntent.putExtra(MovieDetailActivity.EXTRA_MEDIA, mediaList.get(event.getIndex()));
        startActivity(detailActivityIntent);
    }

    private void search(String searchString) {
        new AsyncTask<String, Void, Void>() {
            String searchString;

            @Override
            public void onPreExecute() {
                pbLoading.setVisibility(View.VISIBLE);
                tvEmptyState.setVisibility(View.GONE);
                mediaList.clear();
                movieAdapter.notifyDataSetChanged();
            }

            @Override
            public Void doInBackground(String... params) {
                searchString = params[0];
                TmdbSearch search = new TmdbSearch(Imdb.tmdbApi);
                MovieResultsPage results = search.searchMovie(searchString, null, null, false, null);
                int pages = results.getTotalPages();

                ArrayList<MovieDb> movieResults = (ArrayList<MovieDb>) results.getResults();
                addMovies(movieResults);

                // Search though all page results
                for (int i = 2; i <= pages; i++) {
                    results = search.searchMovie(searchString, null, null, false, i);
                    movieResults = (ArrayList<MovieDb>) results.getResults();
                    addMovies(movieResults);
                }

                return null;
            }

            @Override
            public void onPostExecute(Void result) {
                movieAdapter.notifyDataSetChanged();
                checkEmptyState();

                pbLoading.setVisibility(View.GONE);
            }
        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, searchString);
    }

    private void addMovies(List<MovieDb> movies) {
        for (MovieDb movie : movies) {
            mediaList.add(new Media(
                    movie.getId(),
                    movie.getImdbID(),
                    movie.getTitle(),
                    movie.getReleaseDate(),
                    movie.getPosterPath(),
                    movie.getBackdropPath(),
                    movie.getOverview(),
                    movie.getVoteAverage()));
        }
    }

    private void showSearchDialog() {
        final View view = View.inflate(getActivity(), R.layout.view_search, null);
        etSearch = (EditText) view.findViewById(R.id.et_search);
        etSearch.setOnEditorActionListener(this);

        searchDialog = new AlertDialog.Builder(getActivity())
                    .setTitle(getString(R.string.ad_search_title))
                    .setView(view)
                    .setPositiveButton(getString(R.string.ad_btn_search), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (!validateSearch()) {
                                return;
                            }

                            search(etSearch.getText().toString());
                        }
                    })
                .setNegativeButton(getString(R.string.ad_btn_cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                    })
                    .setCancelable(true).create();

        searchDialog.setCanceledOnTouchOutside(true);
        searchDialog.show();
    }

    private void checkEmptyState() {
        if (!mediaList.isEmpty()) {
            tvEmptyState.setVisibility(View.GONE);
        } else {
            tvEmptyState.setVisibility(View.VISIBLE);
        }
    }

    private boolean validateSearch() {
        if (etSearch.getText().toString().isEmpty()) {
            Toast.makeText(getActivity(),
                    getString(R.string.toast_invalid_search),
                    Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}
