package com.gigasloth.imdb.ui.event;

import android.view.View;

public abstract class ViewClickEvent {
    private View view;

    public ViewClickEvent(View view) {
        this.view = view;
    }

    public View getView() {
        return view;
    }
}