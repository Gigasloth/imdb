package com.gigasloth.imdb;

import android.app.Application;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.Volley;
import com.gigasloth.imdb.db.DbHelper;
import com.gigasloth.imdb.util.LruBitmapCache;
import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.config.TmdbConfiguration;

public class Imdb extends Application {
    public static final String TAG = Imdb.class.getSimpleName();
    public static final String API_KEY = "";
    public static TmdbApi tmdbApi;
    public static TmdbConfiguration config;
    public static Imdb instance;

    // DB
    private static DbHelper dbHelper;

    //Volley
    private RequestQueue queue;
    private ImageLoader imageLoader;

    // Otto
    private static Bus bus = new Bus(ThreadEnforcer.MAIN);

    private static int activeFragment;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        if (dbHelper == null) {
            dbHelper = new DbHelper(this);
        }

        // Authenticate The Movie Database API Key
        new Thread(new Runnable() {
            @Override
            public void run() {
                tmdbApi = new TmdbApi(API_KEY);
                config = tmdbApi.getConfiguration();
            }
        }).start();
    }

    public static Bus getBus() {
        return bus;
    }

    public static Imdb getInstance() {
        return instance;
    }

    public static DbHelper getDbHelper() {
        return dbHelper;
    }

    public static int getActiveFragment() {
        return activeFragment;
    }

    public static void setActiveFragment(int activeFragment) {
        Imdb.activeFragment = activeFragment;
    }

    /**
     * Returns the <code>RequestQueue</code> singleton
     *
     * @return
     *      The existing request queue, or a new one if it didn't exist
     */
    public RequestQueue getRequestQueue() {
        if (queue == null) {
            queue = Volley.newRequestQueue(this);
        }

        return queue;
    }

    /**
     * Returns the <code>ImageLoader</code> singleton
     *
     * @return
     *      The existing image loader, or a new one if it didn't exist
     */
    public ImageLoader getImageLoader() {
        getRequestQueue();
        if (imageLoader == null) {
            imageLoader = new ImageLoader(queue, new LruBitmapCache());
        }

        return imageLoader;
    }

}
