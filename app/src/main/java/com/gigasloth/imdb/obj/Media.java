package com.gigasloth.imdb.obj;

import android.provider.BaseColumns;

import java.io.Serializable;

public class Media implements Serializable {
    private int mediaId;
    private String imdbId;
    private String title;
    private String releaseDate;
    private String posterPath;
    private String backdropPath;
    private String overview;
    private String rating;
    private boolean watched;

    public Media(int mediaId,
                 String imdbId,
                 String title,
                 String releaseDate,
                 String posterPath,
                 String backdropPath,
                 String overview,
                 float rating) {
        this.mediaId = mediaId;
        this.imdbId = imdbId;
        this.title = title;
        this.releaseDate = releaseDate;
        this.posterPath = posterPath;
        this.backdropPath = backdropPath;
        this.overview = overview;
        this.rating = String.valueOf(rating);
    }

    public int getMediaId() {
        return mediaId;
    }

    public String getImdbId() {
        return imdbId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getPosterPath() {
        return posterPath;
    }

    public void setPosterPath(String posterPath) {
        this.posterPath = posterPath;
    }

    public String getBackdropPath() {
        return backdropPath;
    }

    public void setBackdropPath(String backdropPath) {
        this.backdropPath = backdropPath;
    }

    public String getOverview() {
        return overview;
    }

    public void setOverview(String overview) {
        this.overview = overview;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public boolean isWatched() {
        return watched;
    }

    public void setWatched(boolean watched) {
        this.watched = watched;
    }

    public static abstract class Entry implements BaseColumns {
        public static final String TABLE_NAME = "watched";
        public static final String COLUMN_NAME_MEDIA_ID = "media_id";
    }
}
