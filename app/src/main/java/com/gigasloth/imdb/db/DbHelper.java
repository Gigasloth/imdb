package com.gigasloth.imdb.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.sqlite.SQLiteQueryBuilder;

import com.gigasloth.imdb.obj.Media;

public class DbHelper extends SQLiteOpenHelper {
    private static final int VERSION = 1;
    private static final String DATABASE_NAME = "watched.db";

    // Create table fields
    private static final String SQL_CREATE_WATCHED_TABLE =
            "CREATE TABLE " + Media.Entry.TABLE_NAME + " (" +
                    Media.Entry._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    Media.Entry.COLUMN_NAME_MEDIA_ID + " INTEGER NOT NULL)";

    // Projections
    private static final String[] mediaProjection = {
            Media.Entry._ID,
            Media.Entry.COLUMN_NAME_MEDIA_ID
    };

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        // Create tables
        db.execSQL(SQL_CREATE_WATCHED_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + Media.Entry.TABLE_NAME);

        onCreate(db);
    }

    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    public long saveMedia(Media media) {
        ContentValues values = new ContentValues();

        values.put(Media.Entry.COLUMN_NAME_MEDIA_ID, media.getMediaId());

        return getWritableDatabase().insert(Media.Entry.TABLE_NAME, null, values);
    }

    public synchronized boolean isMediaWatched(Media media) {
        SQLiteQueryBuilder qb = new SQLiteQueryBuilder();
        qb.setTables(Media.Entry.TABLE_NAME);
        qb.appendWhere(Media.Entry.COLUMN_NAME_MEDIA_ID + "=" + media.getMediaId());

        Cursor cursor;
        cursor = qb.query(getReadableDatabase(), mediaProjection, null, null, null, null, Media.Entry._ID + " DESC");

        if (cursor == null) {
            return false;
        }

        if (cursor.getCount() < 1) {
            cursor.close();
            return false;
        }
        cursor.close();

        return true;
    }
}
