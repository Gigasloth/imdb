package com.gigasloth.imdb.ui.activity;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;
import com.gigasloth.imdb.Imdb;
import com.gigasloth.imdb.R;
import com.gigasloth.imdb.obj.Media;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MovieDetailActivity extends AppCompatActivity {
    public static final String EXTRA_MEDIA = "media";

    @Bind(R.id.tv_title_detail) TextView tvTitle;
    @Bind(R.id.tv_release_date_detail) TextView tvReleaseDate;
    @Bind(R.id.tv_rating_detail) TextView tvRating;
    @Bind(R.id.tv_overview_detail) TextView tvOverview;
    @Bind(R.id.img_backdrop) ImageView imgBackdrop;
    @Bind(R.id.img_poster_detail) ImageView imgPoster;

    private Media media;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_detail);
        ButterKnife.bind(this);

        media = (Media) getIntent().getSerializableExtra(EXTRA_MEDIA);

        tvTitle.setText(media.getTitle());
        tvReleaseDate.setText(media.getReleaseDate());
        tvRating.setText(media.getRating() + " / 10");
        tvOverview.setText(media.getOverview());

        getImage(media.getBackdropPath(), imgBackdrop);
        getImage(media.getPosterPath(), imgPoster);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        ButterKnife.unbind(this);
    }

    private void getImage(String path, final ImageView imageView) {
        Imdb app = Imdb.getInstance();
        String requestUrl = Imdb.config.getSecureBaseUrl() + "/original/" + path;

        app.getImageLoader().get(requestUrl, new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer imageContainer, boolean isImmediate) {
                Bitmap bitmap;

                // ImageLoader's first pass checks the cache which will return null first time around
                if ((bitmap = imageContainer.getBitmap()) == null) {
                    return;
                }

                imageView.setAlpha(1f);
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onErrorResponse(VolleyError volleyError) {
                Log.e("VOLLEY", volleyError.toString());
            }
        });
    }
}
