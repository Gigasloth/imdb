package com.gigasloth.imdb.ui.event;

public abstract class ListClickEvent {
    private int index;

    public ListClickEvent(int index) {
        this.index = index;
    }

    public int getIndex() {
        return index;
    }
}
